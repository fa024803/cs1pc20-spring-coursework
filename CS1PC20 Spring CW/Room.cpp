#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "Items.cpp"

class Room {
private:
    std::string description;
    std::map<std::string, Room*> exits;
    std::vector<Item> items;

public:
    Room(const std::string& desc) : description(desc) {}

    void AddExit(const std::string& direction, Room* room) {
        exits[direction] = room;
    }

    Room* GetExit(const std::string& direction) const {
        auto it = exits.find(direction);
        if (it != exits.end())
            return it->second;
        else
            return nullptr;
    }

    void AddItem(const Item& item) {
        items.push_back(item);
    }

    const std::vector<Item>& GetItems() const {
        return items;
    }

    const std::string& GetDescription() const {
        return description;
    }
};
