#include <iostream>
#include <vector>
#include <map>
#include <string>

class Item {
private:
    std::string name;
    std::string description;

public:
    Item(const std::string& name, const std::string& desc) : name(name), description(desc) {}
    void Interact() const {
        std::cout << "You interact with the " << name << ". " << description << std::endl;
    }
    std::string GetName() const { return name; }
    std::string GetDescription() const { return description; }
};

class Room {
private:
    std::string description;
    std::map<std::string, Room*> exits;
    std::vector<Item> items;

public:
    Room(const std::string& desc) : description(desc) {}
    void AddExit(const std::string& direction, Room* room) { exits[direction] = room; }
    Room* GetExit(const std::string& direction) const {
        auto it = exits.find(direction);
        if (it != exits.end())
            return it->second;
        else
            return nullptr;
    }
    void AddItem(const Item& item) { items.push_back(item); }
    bool RemoveItem(const std::string& itemName) {
        auto it = std::find_if(items.begin(), items.end(), [&](const Item& item) { return item.GetName() == itemName; });
        if (it != items.end()) {
            items.erase(it);
            return true;
        }
        return false;
    }
    const std::vector<Item>& GetItems() const { return items; }
    const std::string& GetDescription() const { return description; }
};

class Character {
private:
    std::string name;
    int health;
    std::vector<Item> inventory;

public:
    Character(const std::string& name, int health) : name(name), health(health) {}
    void TakeDamage(int damage) {
        health -= damage;
        std::cout << name << " takes " << damage << " damage. Health: " << health << std::endl;
    }
    void AddItem(const Item& item) { inventory.push_back(item); }
    const std::vector<Item>& GetInventory() const { return inventory; }
    std::string GetName() const { return name; }
};

int main() {
    // Create Rooms
    Room enchantedForest("Enchanted Forest");
    Room dragonsLair("Dragon's Lair");
    Room crystalCavern("Crystal Cavern");
    Room witchesDen("Witches' Den");
    Room elvenLibrary("Elven Library");
    Room mermaidGrotto("Mermaid Grotto");
    Room dwarvenForge("Dwarven Forge");
    Room goblinMarket("Goblin Market");
    Room hauntedManor("Haunted Manor");
    Room templeOfTheAncients("Temple of the Ancients");

    // Define exits between rooms
    enchantedForest.AddExit("north", &crystalCavern);
    enchantedForest.AddExit("east", &elvenLibrary);
    enchantedForest.AddExit("south", &dragonsLair);
    enchantedForest.AddExit("west", &witchesDen);

    dragonsLair.AddExit("north", &enchantedForest);
    dragonsLair.AddExit("east", &crystalCavern);
    dragonsLair.AddExit("south", &templeOfTheAncients);
    dragonsLair.AddExit("west", &goblinMarket);

    crystalCavern.AddExit("north", &mermaidGrotto);
    crystalCavern.AddExit("east", &elvenLibrary);
    crystalCavern.AddExit("south", &enchantedForest);
    crystalCavern.AddExit("west", &dragonsLair);

    witchesDen.AddExit("north", &elvenLibrary);
    witchesDen.AddExit("east", &enchantedForest);
    witchesDen.AddExit("south", &templeOfTheAncients);
    witchesDen.AddExit("west", &goblinMarket);

    elvenLibrary.AddExit("north", &dwarvenForge);
    elvenLibrary.AddExit("east", &mermaidGrotto);
    elvenLibrary.AddExit("south", &witchesDen);
    elvenLibrary.AddExit("west", &enchantedForest);

    mermaidGrotto.AddExit("north", &templeOfTheAncients);
    mermaidGrotto.AddExit("east", &dwarvenForge);
    mermaidGrotto.AddExit("south", &crystalCavern);
    mermaidGrotto.AddExit("west", &elvenLibrary);

    dwarvenForge.AddExit("north", &goblinMarket);
    dwarvenForge.AddExit("east", &templeOfTheAncients);
    dwarvenForge.AddExit("south", &mermaidGrotto);
    dwarvenForge.AddExit("west", &elvenLibrary);

    goblinMarket.AddExit("north", &hauntedManor);
    goblinMarket.AddExit("east", &enchantedForest);
    goblinMarket.AddExit("south", &dwarvenForge);
    goblinMarket.AddExit("west", &dragonsLair);

    hauntedManor.AddExit("north", &templeOfTheAncients);
    hauntedManor.AddExit("east", &goblinMarket);
    hauntedManor.AddExit("south", &dragonsLair);
    hauntedManor.AddExit("west", &templeOfTheAncients);

    templeOfTheAncients.AddExit("north", &hauntedManor);
    templeOfTheAncients.AddExit("east", &witchesDen);
    templeOfTheAncients.AddExit("south", &mermaidGrotto);
    templeOfTheAncients.AddExit("west", &dwarvenForge);

    // Create Items
    Item key("Key", "The Key: A gleaming marvel of craftsmanship, its polished surface reflects light with a brilliance that captivates the eye. Its intricate design suggests hidden secrets waiting to be unlocked, while its weight reassures of its sturdy construction.");
    Item sword("Sword", " A divine blade imbued with celestial power, forged from radiant steel that gleams with an ethereal light. It possesses a keen edge that cuts effortlessly through darkness, and its hilt emanates a comforting warmth to those of pure heart.");
    Item Scroll("Scroll", "A magical scroll, inscribed with arcane symbols, possesses the power to channel potent spells, shaping reality at the wielder's command with the mere flick of a finger.");
    Item Ring("Ring", "A mystical ring, infused with ancient enchantments, bestows its wearer with extraordinary powers, granting control over elements of nature and unlocking hidden realms of knowledge.");
    Item Armour("Armour", "Corrupted armor, tainted by sinister forces, radiates an ominous aura, twisting its once protective properties into malevolent energies that ensnare the wearer's mind and soul.");
    Item Bread("Bread", "Nutritious bread, baked from hearty grains, restores health and vigor to adventurers, providing sustenance on their perilous journeys through the realms.");
    Item Bow("Bow", "An Elven bow, crafted with exquisite precision and imbued with ancient magic, whispers secrets of the forest and sings with the wind's guidance, delivering swift and deadly arrows to its wielder's foes.");
    Item Scales("Scales", "Mermaid scales, shimmering with iridescent hues, possess the mystical essence of the ocean depths, granting their bearer enhanced agility and resilience against aquatic perils.");
    Item Dagger("Dagger", "A goblin dagger, crudely forged yet surprisingly sharp, exudes a malevolent aura, carrying with it the treacherous intent of its diminutive wielders.");
    Item Mirror("Mirror", "A tarnished looking glass that reflects distorted images and whispers unsettling secrets to those who dare to gaze into its depths.");

    // Add items to rooms
    enchantedForest.AddItem(key);
    templeOfTheAncients.AddItem(sword);
    dragonsLair.AddItem(Scroll);
    crystalCavern.AddItem(Bread);
    witchesDen.AddItem(Ring);
    elvenLibrary.AddItem(Bow);
    dwarvenForge.AddItem(Armour);
    mermaidGrotto.AddItem(Scales);
    goblinMarket.AddItem(Dagger);
    hauntedManor.AddItem(Mirror);

    std::string playerName;
    std::cout << "Enter your character's name: ";
    std::getline(std::cin, playerName);

    // Create a Character (Player)
    Character player(playerName, 100);

    // Set the starting room
    Room* currentRoom = &enchantedForest;

    // Game loop (basic interaction)
    while (true) {
        std::cout << "Current Location: " << currentRoom->GetDescription() << std::endl;
        std::cout << "Items in the room:" << std::endl;
        for (const Item& item : currentRoom->GetItems()) {
            std::cout << "- " << item.GetName() << std::endl;
        }
        std::cout << "Options: ";
        std::cout << "1. Look around | ";
        std::cout << "2. Interact with an item | ";
        std::cout << "3. Move to another room | ";
        std::cout << "4. View inventory | ";
        std::cout << "5. Quit" << std::endl;
        int choice;
        std::cin >> choice;
        if (choice == 1) {
            // Player looks around (no action required)
            std::cout << "You look around the room." << std::endl;
        }
        else if (choice == 2) {
            // Player interacts with an item in the room
            std::cout << "Enter the name of the item you want to interact with: ";
            std::string itemName;
            std::cin.ignore(); // Ignore newline character from previous input
            std::getline(std::cin, itemName);
            bool itemFound = false;
            for (const Item& item : currentRoom->GetItems()) {
                if (item.GetName() == itemName) {
                    item.Interact();
                    player.AddItem(item);
                    currentRoom->RemoveItem(itemName);
                    itemFound = true;
                    break;
                }
            }
            if (!itemFound) {
                std::cout << "No such item in the room." << std::endl;
            }
        }
        else if (choice == 3) {
            // Player moves to another room
            std::cout << "Enter the direction (e.g., north, south, east, west): ";
            std::string direction;
            std::cin.ignore(); // Ignore newline character from previous input
            std::getline(std::cin, direction);
            Room* nextRoom = currentRoom->GetExit(direction);
            if (nextRoom != nullptr) {
                std::cout << "You move to the next room." << std::endl;
                // Update current room
                currentRoom = nextRoom;
            }
            else {
                std::cout << "You can't go that way." << std::endl;
            }
        }
        else if (choice == 4) {
            // View inventory
            std::cout << "Inventory:" << std::endl;
            const std::vector<Item>& inventory = player.GetInventory();
            if (inventory.empty()) {
                std::cout << "Your inventory is empty." << std::endl;
            }
            else {
                for (const Item& item : inventory) {
                    std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
                }
            }
        }
        else if (choice == 5) {
            // Quit the game
            std::cout << "Goodbye, " << playerName << "!" << std::endl;
            break;
        }
        else {
            std::cout << "Invalid choice. Try again." << std::endl;
        }
    }
    return 0;
}
