#include <iostream>
#include <string>
#include <vector>
#include "Items.cpp"
#include "Room.cpp"
class Character {
private:
	std::string name;
	int health;
	std::vector<Item> inventory;
public:
	Character(const std::string& name, int health);
	void TakeDamage(int damage);
};
class Player : public Character {
private:
	Room location;
public:
	Player(const std::string& name, int health);
};
// Example usage:
//Player player1("Alice", 100);
//player1;.SetLocation(&startRoom); // Set the player's starting room